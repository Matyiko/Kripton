package com.example.kripton.crypto.coin

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kripton.R
import com.example.kripton.crypto.model.Coin
import com.example.kripton.databinding.ActivityFavouriteCoinBinding
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.example.kripton.crypto.dialog.CoinDialog

class FavouriteCoinActivity : AppCompatActivity(),  FavouriteCoinAdapter.CoinAdapterCallback,
    CoinDialog.CoinHandler {

    private lateinit var binding: ActivityFavouriteCoinBinding
    private var favouriteCoins: MutableList<Coin> = mutableListOf()
    private var adapter: FavouriteCoinAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFavouriteCoinBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvImages.layoutManager = GridLayoutManager(this, 1)
        binding.srlImages.setOnRefreshListener { showImages(favouriteCoins) }

    }

    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, FavouriteCoinActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 , intent,
            PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.filled_star)
            .setContentTitle(getString(R.string.limit_reached))
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    override fun onResume() {
        super.onResume()
        showImages(favouriteCoins)
    }
    init{
        downloadFavourite()
    }

    private fun showImages(coins: List<Coin>) {
        adapter = FavouriteCoinAdapter(this, coins.toMutableList())
        binding.rvImages.adapter = adapter
        binding.srlImages.isRefreshing = false
    }

    override fun editCoinClicked(coinToEdit: Coin) {
        val editDialog = CoinDialog()
        val bundle = Bundle()
        bundle.putSerializable(CoinDialog.COIN, coinToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, null)
    }

    override fun updateCoin(coinToUpdate: Coin) {
        adapter!!.uploadFavourite(coinToUpdate)
    }

    private fun downloadFavourite() {
        val db = Firebase.firestore
        val docRef = db.collection(Firebase.auth.currentUser!!.email!!)
         docRef.get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                    if (document != null) {
                        Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                        favouriteCoins.add(document.toObject())
                        for(fav in favouriteCoins) {
                            if (fav.limit!! < fav.currentPrice!!) {
                                sendNotification(fav.name + " reached your limit | Price: " + fav.currentPrice + "$")
                            }
                        }
                            showImages(favouriteCoins)
                    } else {
                        Log.d(TAG, "No such document")
                    }
                }

            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }
    }

    companion object {
        const val TAG = "kotlin.StorageActivity"
    }
}