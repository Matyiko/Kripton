package com.example.kripton.crypto.coin

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kripton.R
import com.example.kripton.crypto.model.Coin
import com.example.kripton.databinding.VhImageBinding
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.squareup.moshi.Json
import java.lang.ref.Reference


class CoinAdapter(
    private val context: Context,
    private val coins: MutableList<Coin>,
    private val favouriteCoins: MutableList<Coin>
)
    : RecyclerView.Adapter<CoinAdapter.ViewHolder>(){

    private val layoutInflater = LayoutInflater.from(context)

    init{
        coins.reverse()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinAdapter.ViewHolder {
        return  ViewHolder(VhImageBinding.inflate(layoutInflater, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val coin = coins[position]
        for (fav in favouriteCoins){
            coins.find{coin -> coin.id == fav.id}?.isFavourite = true
        }
        Glide.with(context).load(coin.image).into(holder.imageView)
        holder.coinName.text = coin.name
        holder.coinPrice.text = "$" + coin.currentPrice.toString()
        if(coin.isFavourite) holder.favourite.setImageResource(R.mipmap.filled_star)
        else holder.favourite.setImageResource(R.mipmap.empty_star)
        holder.favourite.setOnClickListener {
            if(coin.isFavourite) {
                holder.favourite.setImageResource(R.mipmap.empty_star)
                coin.isFavourite = !coin.isFavourite
                deleteFavourite(coin)
            }
            else {
                holder.favourite.setImageResource(R.mipmap.filled_star)
                coin.isFavourite = !coin.isFavourite
                uploadFavourite(coin)
            }
        }
    }

    override fun getItemCount(): Int = coins.size

    class ViewHolder(binding: VhImageBinding) : RecyclerView.ViewHolder(binding.root) {
        val imageView: ImageView = binding.ivImage
        var coinName: TextView = binding.CoinItemNameTextView
        val coinPrice: TextView = binding.CoinItemPriceTextView
        val favourite: ImageButton = binding.imageButton
    }


    private fun uploadFavourite(coin: Coin){
        val db = Firebase.firestore
        val favouriteCoin = hashMapOf(
            "id" to coin.id,
            "symbol" to coin.symbol,
            "name" to coin.name,
            "image" to coin.image,
            "currentPrice" to coin.currentPrice,
            "isFavourite" to coin.isFavourite,
            "limit" to coin.limit
        )

        db.collection(Firebase.auth.currentUser!!.email!!).document(coin.id!!)
            .set(favouriteCoin)
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot added with ID: ${coin.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    private fun deleteFavourite(coin: Coin){
        val db = Firebase.firestore

        db.collection(Firebase.auth.currentUser!!.email!!).document(coin.id!!)
            .delete()
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
            .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }

    companion object {
        const val TAG = "kotlin.StorageActivity"
    }
}
