package com.example.kripton.crypto.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Coin (
    var id: String? = "",
    var symbol: String? = "",
    var name: String? = "",
    var image: String? = "",
    @Json(name = "current_price") val currentPrice: Double? = 0.0,
    var isFavourite: Boolean = false,
    var limit: Double? = 0.0
) : Serializable