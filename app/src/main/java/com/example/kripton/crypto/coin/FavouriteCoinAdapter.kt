package com.example.kripton.crypto.coin

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kripton.crypto.model.Coin
import com.example.kripton.databinding.FavouriteCoinBinding
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class FavouriteCoinAdapter(
    private val context: Context,
    private val coins: MutableList<Coin>,
)
    : RecyclerView.Adapter<FavouriteCoinAdapter.ViewHolder>(){

    private val layoutInflater = LayoutInflater.from(context)

    init{
        coins.reverse()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteCoinAdapter.ViewHolder {
        return  ViewHolder(FavouriteCoinBinding.inflate(layoutInflater, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val coin = coins[position]
        Glide.with(context).load(coin.image).into(holder.imageView)
        holder.coinName.text = coin.name
        holder.coinPrice.text = "$" + coin.currentPrice.toString()
        holder.limit.text = coin.limit.toString()
        holder.limit.setOnClickListener{
            (context as CoinAdapterCallback).editCoinClicked(coin)
            holder.limit.text = coin.limit.toString()
        }
    }

    interface CoinAdapterCallback {
        fun editCoinClicked(coinToEdit: Coin)
    }

    override fun getItemCount(): Int = coins.size

    class ViewHolder(binding: FavouriteCoinBinding) : RecyclerView.ViewHolder(binding.root) {
        val imageView: ImageView = binding.ivImage
        var coinName: TextView = binding.CoinItemNameTextView
        val coinPrice: TextView = binding.CoinItemPriceTextView
        val limit: Button = binding.btnLimit
    }

    fun uploadFavourite(coin: Coin){
        val db = Firebase.firestore
        val favouriteCoin = hashMapOf(
            "id" to coin.id,
            "symbol" to coin.symbol,
            "name" to coin.name,
            "image" to coin.image,
            "currentPrice" to coin.currentPrice,
            "isFavourite" to coin.isFavourite,
            "limit" to coin.limit
        )

        db.collection(Firebase.auth.currentUser!!.email!!).document(coin.id!!)
            .set(favouriteCoin)
            .addOnSuccessListener {
                Log.d(TAG, "DocumentSnapshot added with ID: ${coin.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    companion object {
        const val TAG = "kotlin.StorageActivity"
    }
}