package com.example.kripton.crypto.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.kripton.R
import com.example.kripton.crypto.model.Coin
import com.example.kripton.databinding.DialogCoinBinding


class CoinDialog : DialogFragment() {

    interface CoinHandler {
        fun updateCoin(coinToUpdate: Coin)
    }

    companion object {
        const val COIN = "COIN"
    }

    private lateinit var mBinding: DialogCoinBinding

    private lateinit var coinHandler: CoinHandler

    private var limit: Double
        get() = mBinding.etLimit.text.toString().toDouble()
        set(value) {
            mBinding.etLimit.setText(value.toString())
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CoinHandler) {
            coinHandler = context
        } else {
            throw RuntimeException("The Activity does not implement the TrackHandler interface")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        mBinding = DialogCoinBinding.inflate(LayoutInflater.from(context))
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle(R.string.limit)
                .setView(mBinding.root)
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel) { _, _ -> dialog?.cancel() }
            val coinToEdit = requireArguments().getSerializable(COIN) as Coin
            limit = coinToEdit.limit!!
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
                handleCoinEdit()
                dialog.dismiss()
        }
    }

    private fun handleCoinEdit() {
        val coinToEdit = requireArguments().getSerializable(COIN) as Coin
        coinToEdit.limit = limit
        coinHandler.updateCoin(coinToEdit)
    }
}
