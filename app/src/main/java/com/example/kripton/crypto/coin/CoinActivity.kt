package com.example.kripton.crypto.coin

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kripton.crypto.model.Coin
import com.example.kripton.databinding.ActivityCoinBinding
import com.example.kripton.network.NetworkManager
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class CoinActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCoinBinding
    private var adapter: CoinAdapter? = null
    private val favouriteCoins: MutableList<Coin> = mutableListOf()

    @SuppressLint("StringFormatInvalid")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCoinBinding.inflate(layoutInflater)
        setContentView(binding.root)

        downloadFavourite()

        binding.rvImages.layoutManager = GridLayoutManager(this, 1)
        binding.srlImages.setOnRefreshListener { loadImages() }
    }

    override fun onResume() {
        super.onResume()
        loadImages()
    }

    private fun loadImages() {
        val galleryInteractor = NetworkManager()
        galleryInteractor.getCoins(onSuccess = this::showImages, onError = this::showError)
    }

    private fun showImages(coins: List<Coin>) {
        adapter = CoinAdapter(applicationContext, coins.toMutableList(), favouriteCoins)
        binding.rvImages.adapter = adapter
        binding.srlImages.isRefreshing = false
    }

    private fun showError(e: Throwable) {
        e.printStackTrace()
        binding.srlImages.isRefreshing = false
    }

    private fun downloadFavourite() {
        val db = Firebase.firestore
        val docRef = db.collection(Firebase.auth.currentUser!!.email!!)
        docRef.get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(FavouriteCoinActivity.TAG, "${document.id} => ${document.data}")
                    if (document != null) {
                        Log.d(FavouriteCoinActivity.TAG, "DocumentSnapshot data: ${document.data}")
                        favouriteCoins.add(document.toObject())
                    } else {
                        Log.d(FavouriteCoinActivity.TAG, "No such document")
                    }
                }

            }
            .addOnFailureListener { exception ->
                Log.d(FavouriteCoinActivity.TAG, "get failed with ", exception)
            }
    }
}