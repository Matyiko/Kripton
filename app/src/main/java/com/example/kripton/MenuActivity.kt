package com.example.kripton

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.kripton.crypto.coin.CoinActivity
import com.example.kripton.crypto.coin.FavouriteCoinActivity
import com.example.kripton.databinding.ActivityMainBinding
import com.example.kripton.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCryptos.setOnClickListener{
            startActivity(Intent(this@MenuActivity, CoinActivity::class.java))
        }

        binding.btnMyCryptos.setOnClickListener{
            startActivity(Intent(this@MenuActivity, FavouriteCoinActivity::class.java))
        }
    }
}