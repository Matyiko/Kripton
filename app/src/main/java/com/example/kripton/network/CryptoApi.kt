package com.example.kripton.network

import com.example.kripton.crypto.model.Coin
import retrofit2.Call
import retrofit2.http.GET

interface CryptoApi {
    companion object {
        const val ENDPOINT_URL = "https://api.coingecko.com/api/v3/coins/"

    }

    @GET("markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false")
    fun getCoins(): Call<List<Coin>>
}