package com.example.kripton.network

import android.os.Handler
import android.os.Looper
import com.example.kripton.crypto.model.Coin
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.Call
import retrofit2.converter.moshi.MoshiConverterFactory
import kotlin.concurrent.thread


class NetworkManager {
    private val retrofit: Retrofit
    private val cryptoApi: CryptoApi

    init {
        val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(CryptoApi.ENDPOINT_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
        cryptoApi = retrofit.create(CryptoApi::class.java)
    }

    private fun <T> runCallOnBackgroundThread(
        call: Call<T>,
        onSuccess: (T) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val handler = Handler(Looper.getMainLooper()!!)
        thread {
            try {
                val response = call.execute().body()!!
                handler.post { onSuccess(response) }

            } catch (e: Exception) {
                e.printStackTrace()
                handler.post { onError(e) }
            }
        }
    }

    fun getCoins(
        onSuccess: (List<Coin>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        val getCoinsRequest = cryptoApi.getCoins()
        runCallOnBackgroundThread(getCoinsRequest, onSuccess, onError)
    }
}