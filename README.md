# Kripton

## A Simple crypto currency android app using the CoinGecko API and connected to Firebase

In the app we can see a big list that shows many crypto currencies and there current values and we can mark them as favourites.
In another part of the app we can see a smaller list that contains only our favourite crypto curriences even offline, however that value is not up to date. The up to date values are coming from the CoinGecko API and the stored values of our favourite coins are coming from the Firebase.

## Technologies

* CoinGecko API
* Firebase

## Language
    Kotlin

## Version
    1.0.4